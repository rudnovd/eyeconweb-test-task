import { createApp } from 'vue'
import App from './App.vue'
import './styles/fonts.css'
import './styles/index.css'
import './styles/typography.css'

createApp(App).mount('#app')
